\chapter{Results and Discussion}

In this chapter we present the results of the MD-SRM to the four numerical examples described in Chapter 3. Since we are dealing with systems that oscillate rapidly in small intervals, to improve the accuracy of the results, small step-sizes were chosen by decomposing the interval using  sufficiently large values of $p$ (subintervals). To illustrate the relation of the solutions' accuracy to the change in the step-size, we compare the results from $\bigtriangleup t = 0.1, 0.02, 0.01$ ($p= 100, 500, 1000$) step sizes of the Lorenz system. We observed that increasing the number of subintervals improves the accuracy of the solution. The step-sizes and number of iterations chosen in this study were adequate for the accuracy level of 8 decimal places ($1\times 10^{-8}$). For the Lorenz, Genesio, and R\"{o}ssler systems, we present results for $\bigtriangleup t = 0.01$ and $\bigtriangleup t =0.001$ for Chua's circuit system. We then discretized each interval using 10 collocation points. Then, for all the systems, 20 iterations of the MD-SRM were used. To validate the accuracy of the proposed method, the obtained results were compared to the results from the $4^{th}$ and $5^{th}$ order Runge Kutta based MATLAB in-built solver, \textit{ode45}. 

The Lorenz system was solved in the interval $[0,10]$ decomposed into $1000$ subintervals. Table \ref{table1} gives a numerical comparison of selected results obtained from solving the Lorenz system the using the proposed method and the MATLAB solver \textit{ode45}. 

\begin{table*}[width=0.5\textwidth]\centering
\small
\addtolength{\tabcolsep}{-4pt}
\begin{tabular}{@{}lcccccccc@{}}\toprule 
$t$ & \multicolumn{2}{c}{$x(t)$} & \phantom{abc} & \multicolumn{2}{c}{$y(t)$} & \phantom{abc} & \multicolumn{2}{c}{$z(t)$}\\
\cmidrule{2-3} \cmidrule{5 -6} \cmidrule{8-9} 
& MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45}\\
\midrule \\
$1$ & -12.49383100 & -12.49383100 && -8.77288714 & -8.77288714 && 35.84156013 & 35.84156013 \\ 
$2$ & -1.44435867 & -1.44435867 && -1.07497707 & -1.07497707 && 19.51705719 & 19.51705719\\
$3$ & 4.19448300 & 4.19448300 && 7.30345802 & 7.30345802 && 12.82110170 & 12.82110170\\
$4$ & -14.67507987 & -14.67507987 && -20.18910683 & -20.18910683 && 29.06336146 & 29.06336146\\
$5$ & 2.20845210 & 2.20845210 && -0.56668579 & -0.56668579 && 25.04906597 & 25.04906597\\
$6$ & -2.88302812 & -2.88302812 && -4.76355740 & -4.76355740 && 20. 35555802 & 20. 35555802\\
$7$ & -13.30688245 & -13.30688245 && -16.46514837 & -16.46514837 && 29.76556457 & 29.76556457\\
$8$ & -2.67925263 & -2.67925263 && 1.42947642 & 1.42947642 && 27.10565917 & 27.10565917\\
$9$ & -2.47558624 & -2.47558624 && -4.25730388 & -4.25730389 && 20.20213540 & 20.20213542\\
$10$ & -12.02664497 & -12.02664499 && -17.52028116 & -17.52028115 && 24.30015346 & 24.30015352\\
\bottomrule
\end{tabular}
\caption{Comparison between the numerical results obtained from MD-SRM against the results of \textit{ode45} for the Lorenz system}
\label{table1}
\end{table*}

We observe that the proposed method gives the same results as the \textit{ode45} results upto 8 decimal places ($1\times 10^{-8}$). However, the accuracy tends to decrease as t increases. Figure \ref{lorenz_graphs} shows the graphical comparison of the MD-SRM and \textit{ode45} time series solution for the Lorenz system, whilst Figure \ref{lorenz_portraits} compares the phase portraits obtained from using both methods. We also observe a good agreement between the two numerical methods in both the time series solution and phase portrait comparison. From this results, we can say that the MD-SRM is a valid method for solving the Lorenz system.

In solving the Genesio system, we considered the interval $[0,50]$ decomposed into $5000$ subintervals. Table \ref{table2} gives a numerical comparison of the MD-SRM results obtained from solving the Genesio system against the results from MATLAB's solver \textit{ode45}. The results are in good agreement  upto 8 decimal places ($1 \times 10^{-8}$). Figure \ref{genesio_graphs} shows the comparison of the Genesio time series solution using the MD-SRM and \textit{ode45} results. We observe a compatible relation between the two methods. 

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/LorentzFigures/x.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/LorentzFigures/y.png}
\endminipage\\
\vspace*{0.01cm}
  \includegraphics[width=0.4\linewidth]{images/LorentzFigures/z.png}
\caption{MD-SRM time series for the Lorenz system compared with \textit{ode45} time series}
\label{lorenz_graphs} 
\end{figure}
\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/LorentzFigures/xy.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/LorentzFigures/yz.png}
\endminipage\\ 
\vspace*{0.01cm}
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/LorentzFigures/xz.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/LorentzFigures/xyz.png}
\endminipage
\caption{MD-SRM phase portraits for the Lorenz system compared with \textit{ode45}}
\label{lorenz_portraits} 
\end{figure}

Phase portraits comparison, shown in Figure \ref{genesio_portraits}, also show a very similar trajectory. Hence, the MD-SRM is a valid method for solving the Genesio system. In Table \ref{table3} is the comparison of the numerical results we obtained from solving the R\"{o}ssler system in the interval $[0,100]$ divided into $10000$ subintervals, using the MD-SRM and \textit{ode45}. The results are similar upto 8 decimal places ($1 \times 10^{-8}$). 

\begin{table*}[width=0.5\textwidth]\centering
\small
\addtolength{\tabcolsep}{-4pt}
\begin{tabular}{@{}lcccccccc@{}}\toprule 
$t$ & \multicolumn{2}{c}{$x(t)$} & \phantom{abc} & \multicolumn{2}{c}{$y(t)$} & \phantom{abc} & \multicolumn{2}{c}{$z(t)$}\\
\cmidrule{2-3} \cmidrule{5 -6} \cmidrule{8-9} 
& MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45}\\
\midrule \\
$5$ & -0.17746819 & -0.17746819 && 0.25478458 & 0.25478458 && 0.78019195 & 0.78019195 \\ 
$10$ & 0.54927891 & 0.54927891 && -0.79456554 & -0.79456554 && -2.16563487 & -2.16563487\\
$15$ & -1.28388549 & -1.28388549 && 2.45604490 & 2.45604490  && 6.93586848 & 6.93586848\\
$20$ & 5.58245837 & 5.58245837 && -0.63743566 & -0.63743566 && -8.46661069 & -8.46661069\\
$25$ & -0.61000566 & -0.61000566 && -4.96290657 & -4.96290657 && 2.09679076 & 2.09679076\\
$30$ & -1.35840166 & -1.35840166 && 1.70232373 & 1.70232373 && 7.59511436 & 7.59511436\\
$35$ & 5.55932461 & 5.55932461 && 0.08401779 & 0.08401779 && -9.25299603 & -9.25299603\\
$40$ & -0.23473662 & -0.23473662 && -5.13895874 & -5.13895874 && 1.04274016 & 1.04274016\\
$45$ & -1.44052039 & -1.44052039 && 0.83337101 & 0.83337101 && 7.56762027 & 7.56762027\\
$50$ & 5.40552999 & 5.40552999 && 0.92312848 & 0.92312848 && -9.94539698 & -9.94539698\\
\bottomrule
\end{tabular}
\caption{Comparison between the results obtained from MD-SRM against the results of \textit{ode45} for the Genesio system}
\label{table2}
\end{table*}

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/genisioFigures/x.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/genisioFigures/y.png}
\endminipage
\end{figure}

\begin{figure}[H]\centering
  \includegraphics[width=0.4\linewidth]{images/genisioFigures/z.png}
\caption{MD-SRM time series solution for the Genisio system compared with \textit{ode45} time series}
\label{genesio_graphs} 
\end{figure}   

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/genisioFigures/xy.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/genisioFigures/yz.png}
\endminipage \\
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/genisioFigures/xz.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/genisioFigures/xyz.png}
\endminipage
\caption{MD-SRM phase portraits for the Genisio system compared with \textit{ode45} phase portraits}
\label{genesio_portraits} 
\end{figure}
    

\begin{table*}[width=0.5\textwidth]\centering
\small
\addtolength{\tabcolsep}{-4pt}
\begin{tabular}{@{}lcccccccc@{}}\toprule 
$t$ & \multicolumn{2}{c}{$x(t)$} & \phantom{abc} & \multicolumn{2}{c}{$y(t)$} & \phantom{abc} & \multicolumn{2}{c}{$z(t)$}\\
\cmidrule{2-3} \cmidrule{5 -6} \cmidrule{8-9} 
& MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45}\\
\midrule \\
$10$ & 0.05478069 & 0.05478069 && -0.11622406 & -0.11622406 && 0.03533573 & 0.03533573\\ 
$20$ & -0.20350522 & -0.20350522 && 0.09716658 & 0.09716658 && 0.03403345 & 0.03403345\\
$30$ & 0.634731618 & 0.634731618 && -0.07433818 & -0.07433818 && 0.03926550 & 0.03926550\\
$40$ & -1.38930145 & -1.38930145 && -0.77839723 & -0.77839723 && 0.02792051 & 0.02792051\\
$50$ & 1.97347099 & 1.97347099 && 3.58395562 & 3.58395562 && 0.07188105 & 0.07188105\\
$60$ & 1.13402167 & 1.13402167 && -10.49976398 & -10.49976398 && 0.03330174 & 0.03330174\\
$70$ & -5.28590788 & -5.28590788 && -1.62060709 & -1.62060709 && 0.01804300 & 0.01804300\\
$80$ & -1.84290635 & -1.84290635 && 1.82289156 & 1.82289156 && 0.73236485 & 0.73236485\\
$90$ & 6.59209761 & 6.59209761 && -1.95665773 & -1.95665773 && 0.20936609 & 0.20936609\\
$100$ & -0.18973324 & -0.18973324 && -4.01294698 & -4.01294698 && 0.03106376 & 0.03106376\\
\bottomrule
\end{tabular}
\caption{Comparison between the results obtained from MD-SRM against the results of \textit{ode45} for the R\"{o}ssler system}
\label{table3}
\end{table*}

Graphically, the time series MD-SRM solution is being compared to the solution obtained from \textit{ode45} in Figure \ref{rossler_graphs}. We observe a very good agreement between the two methods. Figure \ref{rossler_portraits} shows the comparison of the phase portraits obtained from both the MD-SRM and \textit{ode45} for the R\"{o}ssler system. The portraits have the same trajectories, hence validating the use of the MD-SRM in solving the R\"{o}ssler system. 

Table \ref{table4} shows a numerical comparison of selected results obtained from solving Chua's circuit system using MD-SRM and the MATLAB solver \textit{ode45} in the interval $[0,10]$ divided into $1000$ subintervals. The results are similar upto 8 decimal places ($1\times 10^{-8}$) for small values of $t$. As $t$ increases, these level of accuracy decreases. Figure \ref{chua_graphs} shows the comparison of the time series MD-SRM solution to the \textit{ode45} solution. The results are in good agreement. 

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/RosslerFigures/x.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/RosslerFigures/y.png}
\endminipage
\vspace*{0.01cm}
  \includegraphics[width=0.4\linewidth]{images/RosslerFigures/z.png}
\caption{MD-SRM time series solution for the R\"{o}ssler system compared with \textit{ode45} time series}
\label{rossler_graphs} 
\end{figure}

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.7\linewidth]{images/RosslerFigures/xy.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.7\linewidth]{images/RosslerFigures/yz.png}
\endminipage \\
\minipage{0.5\textwidth}
  \includegraphics[width=0.7\linewidth]{images/RosslerFigures/xz.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.7\linewidth]{images/RosslerFigures/xyz.png}
\endminipage
\caption{MD-SRM phase portraits for the R\"{o}ssler system compared with \textit{ode45} phase portraits}
\label{rossler_portraits} 
\end{figure}

\begin{table*}[width=0.5\textwidth]\centering
\small
\addtolength{\tabcolsep}{-4pt}
\begin{tabular}{@{}lcccccccc@{}}\toprule 
$t$ & \multicolumn{2}{c}{$x(t)$} & \phantom{abc} & \multicolumn{2}{c}{$y(t)$} & \phantom{abc} & \multicolumn{2}{c}{$z(t)$}\\
\cmidrule{2-3} \cmidrule{5 -6} \cmidrule{8-9} 
& MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45} && MD-SRM & \textit{ode45}\\
\midrule \\
$10$ & 1.25880888 & 1.25880889 && -0.25101670 & -0.25101669 && -2.12019311 & -2.12019312\\ 
$20$ & 2.13279494 & 2.13279492 && 0.30095053 & 0.30095053 && -2.30253378 & -2.30253370\\
$30$ & -1.02280323 & -1.02280312 && -0.12688486 & -0.12688486 && 1.79088402 & 1.79088392\\
$40$ & -1.55218831 & -1.55218831 && -0.12072101 & -0.12072100 && 0.97022619 & 0.97022627\\
$50$ & -1.00788134 & -1.00788139 && 0.15581464 & 0.15581462 && 0.59118582 & 0.59118590\\
$60$ & -1.64919814 & -1.64919811 && 0.14341720 & 0.14341719 && 2.93331265 & 2.93331247\\
$70$ & -1.39948628 & -1.39948742 && -0.27816846 & -0.27816857 && 0.66205591 & 0.66205710\\
$80$ & -0.06212060 & -0.06211920 && 0.27212241 & 0.27212239 && -0.48448675 & -0.48448859\\
$90$ & 1.49971681 & 1.49971615 && 0.08951540 & 0.08951493 && -0.99279780 & -0.99279793\\
$100$ & 1.10366517 & 1.10366647 && -0.13914912 & -0.13914894 && 0.85110869 & 0.85111249\\
\bottomrule
\end{tabular}
\caption{Comparison between the results obtained from MD-SRM against the results of \textit{ode45} for Chua's circuit system}
\label{table4}
\end{table*}

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/chuafigures/x.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/chuafigures/y.png}
\endminipage
\end{figure}

\begin{figure}[H]\centering
  \includegraphics[width=0.5\linewidth]{images/chuafigures/z.png}
\caption{MD-SRM time series solution for Chua's circuit system compared with \textit{ode45} time series solution}
\label{chua_graphs} 
\end{figure}

\begin{figure}[H]\centering 
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/chuafigures/xy.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/chuafigures/yz.png}
\endminipage \\
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/chuafigures/xz.png}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=0.8\linewidth]{images/chuafigures/xyz.png}
\endminipage
\caption{MD-SRM phase portraits for Chua's circuit system compared with \textit{ode45}}
\label{chuas_portraits} 
\end{figure}

Figure \ref{chuas_portraits} shows the comparison between the phase portraits obtained from the MD-SRM solution and the ones obtained from the \textit{ode45} solution. The trajectories from both methods are similar, once again proving the validity of the proposed method. Table \ref{table5} shows comparison of $x(t)$ results as the number of subintervals ($p$) is increased when solving the Lorenz system. Considering $p =100, 500, 1000$, we observe that as the step size increases, the solution converges to an accuracy level of 8 decimal places. 

\begin{table}[H]\centering
\begin{tabular}{@{}lccccc@{}}\toprule 
$t$ && \multicolumn{4}{c}{$x(t)$}\\
\cmidrule{3-6} 
&& MD-SRM $(p=100)$ & MD-SRM $(p=500)$ & MD-SRM $(p=1000)$&\textit{ode45}\\
\midrule \\
$1$ &&-12.49382830 & -12.49383100 & -12.49383100 & -12.49383100  \\ 
$2$&&-1.44436112 & -1.44435867 & -1.44435867 & -1.44435867\\
$3$ && 4.19447506 & 4.19448300 & 4.19448300 & 4.19448300\\
$4$ && -14.675515143 & -14.67507987 & -14.67507987 & -14.67507987 \\
$5$ && 2.20841996 & 2.20845210 & 2.20845210 & 2.20845210\\
$6$ && -2.88293964 & -2.88302812 & -2.88302812 & -2.88302812\\
$7$ && -13.30669980 & -13.30688245 & -13.30688245 & -13.30688245\\
$8$ && -2.67838202 & -2.67925263 & -2.67925263 & -2.67925263\\
$9$ && -2.47566210 & -2.47558624 & -2.47558624 & -2.47558624\\
$10$ && -12.03375662 & -12.02664497 & -12.02664497 & -12.02664499\\
\bottomrule
\end{tabular}
\caption{Comparison of $x(t)$ numerical results obtained from MD-SRM for various step-sizes for the Lorenz system}
\label{table5}
\end{table}

This suggests that $p = 1000$ subintervals, giving $\bigtriangleup t = 0.01$ step size, is sufficient for the accuracy level considered in this work. Table \ref{table6} shows $y(t)$ results obtained as we vary $p = 100, 500, 1000$, when solving the Lorenz system. We observe that as the number of subintervals increases, the accuracy level increases and converges to 8 decimal places. Hence the $1000$ subintervals of the domain $[0,10]$ is adequate for the accuracy level considered in this work.

\begin{table}[H]\centering
\begin{tabular}{@{}lccccc@{}}\toprule 
$t$ && \multicolumn{4}{c}{$y(t)$}\\
\cmidrule{3-6} 
&& MD-SRM $(p=100)$ & MD-SRM $(p=500)$ & MD-SRM $(p=1000)$&\textit{ode45}\\
\midrule \\
$1$  && -8.77288261 & -8.77288714 & -8.77288714 & -8.77288714\\ 
$2$ && -1.07498247 & -1.07497707 & -1.07497707 & -1.07497707\\
$3$ && 7.30344727 & 7.30345802 & 7.30345802 & 7.30345802\\
$4$ && -20.18901264 & -20.18910683 & -20.18910683 & -20.18910683\\
$5$  && -0.56674551 & -0.56668579 & -0.56668579 & -0.56668579\\
$6$  && -4.76356422 & -4.76355740 & -4.76355740 & -4.76355740\\
$7$  && -16.46451443 & -16.46514837 & -16.46514837 & -16.46514837\\
$8$  && 1.42841019 & 1.42947642 & 1.42947642 & 1.42947642\\
$9$  && -4.25895572 & -4.25730388 & -4.25730388 & -4.25730389\\
$10$ && -17.51693248 & -17.52028116 & -17.52028116 & -17.52028115\\
\bottomrule
\end{tabular}
\caption{Comparison between $y(t)$ numerical results obtained from MD-SRM for various step-sizes for the Lorenz system}
\label{table6}
\end{table}

Table \ref{table7} presents the $z(t)$ results obtained when the Lorenz system is solved in the domain $[0,10]$ divided into various number of subintervals; $p=100,500,1000$. Once again, we observe that the accuracy level increases and converges to 8 decimal places as the number of subintervals are increased to $1000$.
 
\begin{table}[H]\centering
\begin{tabular}{@{}lccccc@{}}\toprule 
$t$ && \multicolumn{4}{c}{$z(t)$}\\
\cmidrule{3-6} 
&& MD-SRM $(p=100)$ & MD-SRM $(p=500)$ & MD-SRM $(p=1000)$&\textit{ode45}\\
\midrule \\
$1$   && 35.84155832 & 35.84156013 & 35.84156013 & 35.84156013\\ 
$2$  && 19.51705156 &19.51705719 & 19.51705719 & 19.51705719\\
$3$  && 12.82108177 & 12.82110170 & 12.82110170 & 12.82110170\\
$4$  && 29.06370810 & 29.06336146 & 29.06336146 & 29.06336146\\
$5$   && 25.04908922 & 25.04906597 & 25.04906597 & 25.04906597\\
$6$   && 20. 35647231 & 20. 35555802 & 20. 35555802 & 20. 35555802\\
$7$   && 29.76583680 & 29.76556457 & 29.76556457 & 29.76556457\\
$8$   && 27.10283230 & 27.10565917 & 27.10565917 & 27.10565917\\
$9$   && 20.21270690 & 20.20213540 & 20.20213540 & 20.20213542\\
$10$  && 24.32998725 & 24.30015347 & 24.30015346 & 24.30015352\\
\bottomrule
\end{tabular}
\caption{Comparison of MD-SRM $z(t)$ numerical results using various step-sizes for the Lorenz system}
\label{table7}
\end{table}

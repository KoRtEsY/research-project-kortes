\chapter{Multi-Domain Spectral Relaxation Method (MD-SRM)}

In this section, we present the proposed algorithm for solving first order Initial Value Problems (IVPs) modelling chaotic systems. Firstly, the equations are decoupled using a Gauss-Siedel type of relaxation technique, then the resulting equations are solved using the Chebyshev spectral collocation method (CSCM). 

Generally, chaotic systems can be described by a system of $m$ non-linear first order ordinary differential equations, with $m$ unknown variables dependent on $t$. Hence, to relay the basic principles of the MD-SRM, we consider the system 

\begin{align}\label{syt2.1}
\frac{dx_1(t)}{dt} + \sum_{s=1}^m \alpha_{1,s} x_s(t) + &N_1[x_1(t), x_2(t), \ldots, x_m(t)] = H_1\nonumber\\
\frac{dx_2(t)}{dt} + \sum_{s=1}^m \alpha_{2,s} x_s(t) + &N_2[x_1(t), x_2(t), \ldots, x_m(t)] = H_2\nonumber\\
&\vdots\nonumber\\
\frac{dx_m(t)}{dt} + \sum_{s=1}^m \alpha_{m,s} x_s(t) + &N_m[x_1(t), x_2(t), \ldots, x_m(t)] = H_m\nonumber\\
\end{align}

subject to 

\begin{align}\label{IC:1}
x_s(0) = \beta_s, \qquad s = 1,2,\ldots,m 
\end{align}

where $H_s$ are constants, $N_s$ are non-linear functions of each $i^{th}$ equation for $i = 1,2,\ldots, m$, and the inputs $\alpha_{m,s}$ are known constant parameters.


To solve Equation (\ref{syt2.1}) using the multi-domain technique, we divide the interval of interest, $\Omega =[0,t_p]$ into $p$ sequential non-overlapping subintervals, $\Omega_k = [t_{k-1}, t_k]$, $k = 1,2,\ldots,p$. Each interval $\Omega_k$ is further divided into $N$ equal divisions as illustrated in Figure \ref{grid} below. 

\begin{figure}[H]\centering \small
\begin{tikzpicture}
    \draw (-2,0) -- (2,0);
    \draw[dashed](2,0) -- (4,0);
      \draw(4,0) -- (5,0);    
    \draw[dashed](5,0) -- (7,0);
       \draw(7,0) -- (8,0);    
%    % the snaked line is drawn to 11.1 to force
%    % TikZ to draw the final tick.
    \draw[snake=ticks,segment length=1cm] (-2,0) -- (2.1,0);
    \draw[snake=ticks,segment length=1cm] (4,0) -- (5.1,0);
    \draw[snake=ticks,segment length=1cm] (7,0) -- (8.1,0);
    
  %PUT TEXT BELOW THE LINE
 \node[below] at (-2,0) {$t_0$};% label the hinge
 \node[below] at (-1,0) {$t_1$};
 \node[below] at (0,0) {$t_2$};
  \node[below] at (1,0) {$t_3$};
 \node[below] at (4,0) {$t_{k-1}$};
 \node[below] at (5,0) {$t_k$};
 \node[below] at (7,0) {$t_{p-1}$};
 \node[below] at (8,0) {$t_{p}$};
 \node[above] at (-1.5,0) {$\Omega_1$};
 \node[above] at (-0.5,0) {$\Omega_2$};
 \node[above] at (0.5,0) {$\Omega_3$};
 \node[above] at (4.5,0) {$\Omega_k$};
 \node[above] at (7.5,0) {$\Omega_p$}; 
 % DRAW ELLIPSE
 \draw[black,thick,dashed] (4.5,0) ellipse (1cm and 0.75cm);
 \draw[black,thick,dashed] (4,-2.3) ellipse(4cm and 1cm);

 %DRAW DOWNWARD ARROW
 \draw[->,dashed,black,thick](4.5,-0.8) -- (4.5,-1.3);

%----------LOWER AXIS--------------%
      \draw (1,-2.3) -- (3,-2.3);
      \draw[dashed](3,-2.3) -- (5,-2.3);
      \draw(5,-2.3) -- (7,-2.3);    
%     %snaks
      \draw[snake=ticks,segment length=1cm] (1,-2.3) -- (3.1,-2.3);
      \draw[snake=ticks,segment length=1cm] (5,-2.3) -- (6.1,-2.3);
  %PUT TEXT BELOW THE LINE
 \node[above] at (1,-2.3) {$t_{k-1}$};% label the hinge
 \node[above] at (7,-2.3) {$t_k$};
 \node[below] at (1,-2.3) {$t_{0}^{(k)}$};% label the hinge
 \node[below] at (2,-2.3)  {$t_{1}^{(k)}$};
 \node[below] at (3,-2.3)  {$t_{2}^{(k)}$};
 \node[below] at (6,-2.3)  {$t_{N-1}^{(k)}$};
 \node[below] at (7,-2.3)  {$t_{N}^{(k)}$};
 \end{tikzpicture}
\caption{Multi-domain grid}\label{grid}
\end{figure}

Let $x_s^k$ for $s = 1, 2,\ldots,m$ be the solutions for each unknown variable obtained in the interval $\Omega_k$. The solution procedure assumes that the solution can be approximated by a Lagrange interpolation polynomial of the form

\begin{equation}\label{e2}
x_s^{k}(t)\approx \sum_{i=0}^{N}x_s^{k}(\tau_i^k)L_i(t) \approx
\sum_{i=0}^{N}x^{k}_{i,s}L_i(t),
\end{equation}
for $s = 1,2,\ldots,m$, which interpolates $x_s^{k}(t)$ at selected points defined by the Chebychev-Gauss-Lobatto collocation nodes 

\begin{align}\label{e3}
\tau_i = \cos \left( \frac{\pi i}{N} \right), \qquad i=0,1,2, \ldots,N.
\end{align}

The choice of the Chebyshev-Gauss-Lobatto grid points (\ref{e3}), ensures that there is a simple conversion of the continuous derivatives in time, to discrete derivatives at the grid points. The function
$L_i(t)$ is the characteristic Lagrange cardinal polynomial

\begin{align}
L_i(t) = \prod_{i=0,~i\neq z}^N \frac{t - \tau_z}{\tau_i - \tau_z},
\end{align}

where

\begin{align}
L_i(\tau_z) = \delta_{iz} = \begin{cases}0 \quad &\text{if}~ i\neq z\\
1 \quad &\text{if}~ i  = z \end{cases}
\end{align}


According to \citet{motsa2013new}, the appropriate initial approximation for the systems considered in this study is defined as follows

\begin{align}\label{eq:2.2}
x_{i,0}^k (t) = \begin{cases} 
\beta_{i} \quad &\text{if}~ k=1\\
x_i^{k-1}(t_{k-1}) \quad & \text{if}~ 2 \leq k \leq p.
\end{cases}
\end{align}

That is, for the first subinterval, $\Omega_1 = [0, t_1]$, the initial approximation used are the initial conditions defined in (\ref{IC:1}). In the subsequent intervals, $\Omega_{k} = [t_{k-1}, t_{k}]$ for $k = 2, \ldots, p$, the initial approximation used are the solutions obtained from the previous subinterval, $\Omega_{k-1}$. Starting from an initial approximation $x_{1,0}, x_{2,0}, x_{3,0}, \ldots, x_{m,0}$, the iterative technique for each interval is given by the system

\begin{align}\label{syst2}
\dot{x}_{1,r+1}^k +\sum_{s=1}^m \alpha_{1,s}x_{s,r+1}^k + &N_1[x_{1,r}^k, x_{2,r}^k,\ldots, x_{m,r}^k] = H_1,\nonumber\\
\dot{x}_{2,r+1}^k + \sum_{s=1}^m \alpha_{2,s}x_{s,r+1}^k + &N_2[x_{1,r+1}^k, x_{2,r}^k,\ldots x_{m,r}^k] = H_2,\nonumber\\
&\vdots\nonumber\\
\dot{x}_{m,r+1}^k + \sum_{s=1}^m \alpha_{m,s}x_{s,r+1}^k + &N_m[x_{1,r+1}^k, x_{2,r+1}^k, \ldots, x_{m,r}^k] = H_m,\nonumber\\
\end{align}

subject to the initial conditions

\begin{align}
x_{s, r+1}^k (t_{k-1}) = x_{s,r}^{k-1}(t_{k-1}), \qquad s = 1,2,\ldots,m
\end{align}

where $x_{s, r+1}$ are the approximations of $x_s$ at the current iteration, and  $x_{s, r}$ are the approximations of $x_s$ at the previous iteration. It is worth noting that the now linear system of equations (\ref{syst2}), employs the Gauss-Seidel decoupling technique. That is, for each iteration, $x_1$ is to be solved first using the initial approximation values, then the obtained value of $x_1$ used in solving for $x_2$. Similarly, the obtained values of $x_1$ and $x_2$ are to be used in solving for $x_3$. The same procedure applies in solving for $x_4, x_5, \ldots, x_m$. The iterative scheme, (\ref{syst2}) is then evaluated at the grid points $\tau_j$ for $j = 0,1,2, \ldots, N$ for each $k^{th}$ sub-interval. Since 

%The decoupled iterative system (\ref{syst2}) can be solved using any suitable standard method. However, we are dealing with chaotic systems. That is, the functions are not smooth, hence, the method used should be chosen and applied appropriately. According to \citep{canuto2012spectral} and \citep{trefethen2000spectral}, when solving a smooth function in a simple domain, spectral methods can achieve up-to five times the accuracy of the top two numerical solvers; finite difference and finite element methods. Thus, in considerate of accuracy and quick convergence, we solve the system in very small intervals using the Chebyshev pseudo-spectral method which further discretizes the subintervals using collocation points.  


\begin{equation}\label{e2}
x_s^{k}(t)\approx \sum_{i=0}^{N}x_s^{k}(\tau_i)L_i(t) \approx
\sum_{i=0}^{N}x^{k}_{i,s}L_i(t),
\end{equation} 

then in each interval, the derivatives are approximated by

\begin{align}\label{eq:2.3}
\frac{dx_{s}^k}{d t}(t)\bigg|_{t=\tau_{j}} &= \sum_{i=0}^{N}x_s^{k}(\tau_i)\frac{dL_i(\tau_j)}{dt} = \frac{2}{t_k-t_{k-1}}\sum_{i=0}^{N}x_s^{k}(\tau_i) \mathcal{D}_{ji}\nonumber\\ 
&=\frac{2}{t_k-t_{k-1}}\sum_{i=0}^{N} \mathcal{D}_{ji} x_s^{k}(\tau_i) = \sum_{i=0}^N D_{ji} x_s^k(\tau_i)\nonumber\\
\end{align}

where $D_{ji}= 2\mathcal{D}_{ji}/(t_k-t_{k-1})$ and $\mathcal{D}_{ji} = \frac{dL_i(\tau_j)}{dt}$ is the $j^{th}$ and $i^{th}$ entry of the $(N+1) \times (N+1)$ standard first derivative Chebyshev differential matrix described in detail by ~\citet{trefethen2000spectral}. Collocating, that is evaluating Equation (\ref{syst2}) at $\tau_j$ gives

\begin{align}\label{syst3}
\sum_{i=0}^N D_{ji} x_{1,r+1}^k(\tau_i) + \sum_{s=1}^m \alpha_{1,s}x_{s,r+1}^k(\tau_j)  + &N_1[x_{1,r}^k(\tau_j), x_{2,r}^k(\tau_j),\ldots, x_{m,r}^k(\tau_j)] = H_1,\nonumber\\
\sum_{i=0}^N D_{ji} x_2^k(\tau_i) + \sum_{s=1}^m \alpha_{2,s}x_{s,r+1}^k(\tau_j)  + &N_2[x_{1,r+1}^k(\tau_j), x_{2,r}^k(\tau_j),\ldots, x_{m,r}^k(\tau_j)] = H_2,\nonumber\\
&\vdots\nonumber\\
\sum_{i=0}^N D_{ji} x_m^k(\tau_i) + \sum_{s=1}^m \alpha_{m,s}x_{s,r+1}^k(\tau_j) + &N_m[x_{1,r+1}^k(\tau_j), x_{2,r+1}^k(\tau_j), \ldots, x_{m,r}^k(\tau_j)] = H_m.\nonumber\\
\end{align} 

Since   
\begin{align}
x_{s,r+1}^k(0) = x_{s,r+1}^k(t_N),
\end{align}

then Equation (\ref{syst3}) simplifies to 

\begin{align}\label{syst4}
\sum_{i=0}^{N-1} D_{ji} x_{1,r+1}^k(\tau_i) + \alpha_{1,1} x_{1,r+1}^k(\tau_j) &= H_1 - D_{jN} x_{1,r+1}^k(\tau_N) - \bigg((1-\delta_{1,s})\sum_{s=1}^m \alpha_{1,s}x_{s,r+1}^k 
\nonumber\\ &+ N_1[x_{1,r}^k(\tau_j), x_{2,r}^k(\tau_j),\ldots, x_{m,r}^k(\tau_j)]\bigg),\nonumber\\
\sum_{i=0}^{N-1} D_{ji} x_{2, r+1}^k(\tau_i) + \alpha_{2,2} x_{2,r+1}^k(\tau_j) &= H_2 - D_{jN} x_{2,r+1}^k(\tau_N) - \bigg((1-\delta_{2,s})\sum_{s=1}^m \alpha_{2,s}x_{s,r+1}^k 
\nonumber\\ & + N_2[x_{1,r+1}^k(\tau_j), x_{2,r}^k(\tau_j), \ldots, x_{m,r}^k(\tau_j)]\bigg),\nonumber\\
&\vdots\nonumber\\
\sum_{i=0}^{N-1} D_{ji} x_{m,r+1}^k(\tau_i) + \alpha_{m,m}x_{m,r+1}^k(\tau_j) &= H_m - D_{jN} x_{m,r+1}^k(\tau_N) - \bigg((1-\delta_{m,s})\sum_{s=1}^m \alpha_{m,s}x_{s,r+1}^k(\tau_j) 
\nonumber\\ &+ N_m[x_{1,r+1}^k(\tau_j), x_{2,r+1}^k(\tau_j), \ldots, x_{m,r}^k(\tau_j)]\bigg) \nonumber\\
\end{align} 

for $j=0,1,\ldots, N-1$, and the Kronecker delta $\delta_{m,s}$. Varying $j=0,1,\ldots,N-1$, we obtain

%Application of the CSCM to \ref{syst4} and using the definitions from Equations \ref{eq:2.3}, for each $k$th interval we have
%
%\begin{align}
%\textbf{X}_{i, r+1}^k(\tau_N^{k-1}) = \textbf{X}_{i}^{k-1}(\tau_N^{i-1}),
%\end{align}
%%
%and the iterative technique 

\begin{align}\label{system2}
[\textbf{D} + \alpha_{1,1}\textbf{I}]\textbf{X}_{1, r+1}^k = \textbf{H}_1&  -\bigg((1-\delta_{1,s})\sum_{s=1}^m \alpha_{1,s}\textbf{X}_{s,r+1}^k + N_1[\textbf{X}_{1,r}^k, \textbf{X}_{2,r}^k, \ldots \textbf{X}_{m,r}^k]\bigg) \nonumber\\ & - D_{jN} \textbf{X}_{1,r+1}^k(\tau_N),\nonumber\\
[\textbf{D} + \alpha_{2,2}\textbf{I}]\textbf{X}_{2, r+1}^k = \textbf{H}_2&- \bigg((1-\delta_{2,s})\sum_{s=1}^m \alpha_{2,s}\textbf{X}_{s,r+1}^k + N_2[\textbf{X}_{1,r+1}^k, \textbf{X}_{2,r}^k, \ldots, \textbf{X}_{m,r}^k]\bigg)\nonumber\\ & - D_{jN} \textbf{X}_{2,r+1}^k(\tau_N),\nonumber\\
&\vdots\nonumber\\
[\textbf{D} + \alpha_{m,m}\textbf{I}]\textbf{X}_{m, r+1}^k = \textbf{H}_m&- \bigg((1-\delta_{m,s})\sum_{s=1}^m \alpha_{m,s}\textbf{X}_{s,r+1}^k + N_m[\textbf{X}_{1,r+1}^k, \textbf{X}_{2,r+1}^k, \ldots, \textbf{X}_{m,r}^k]\bigg)\nonumber\\ & - D_{jN} \textbf{X}_{m,r+1}^k(\tau_N), \nonumber\\ 
\end{align}
%
where $\textbf{I}$ is an $N \times N$ identity matrix and $\textbf{H}_s$ for $s=1,2,\ldots,m$ are column vectors. We can re-write each equation in \eqref{system2} in compact form as follows

\begin{align}\label{eq:2.5}
\textbf{A}_s \textbf{X}_{s, r+1}^k = \textbf{R}_s^k,
\end{align} 

where 
\begin{align}
\textbf{A}_s &= \textbf{D} + \alpha_{s,s}\textbf{I}\\
\text{and}~~ \textbf{R}_s &= \textbf{H}_s - D_{jN} x_{s,r+1}^k(\tau_N) -\bigg((1-\delta_{s,i})\sum_{i=1}^m \alpha_{s,i}\textbf{X}_{i,r+1}^k + N_s[\textbf{X}_{1,r+1}^k, \textbf{X}_{2,r+1}^k, \ldots, \textbf{X}_{m,r}^k]\bigg). 
\end{align}

Expressing Equation (\ref{eq:2.5}) in matrix form we have

\begin{align}\label{generalmatrix}
\begin{bmatrix}
D_{00} + \alpha_{s,s} & D_{01} &  \cdots & D_{0~N-1}\\
D_{10} & D_{11} + \alpha_{s,s} &  \cdots & D_{1~N-1}\\
D_{20} & D_{21} &  \cdots & D_{2~N-1}\\
\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & \cdots & D_{N-1~N-1} + \alpha_{s,s}
\end{bmatrix} \begin{bmatrix}
x_{s,r+1}^k(\tau_0)\\
x_{s,r+1}^k(\tau_1)\\
x_{s,r+1}^k(\tau_2)\\
\vdots\\
x_{s,r+1}^k(\tau_{N-1})
\end{bmatrix} ~ = \begin{bmatrix}
R_{s,r}^k(\tau_0)\\
R_{s,r}^k(\tau_1)\\
R_{s,r}^k(\tau_2)\\
\vdots\\
R_{s,r}^k(\tau_{N-1})\\
\end{bmatrix},
\end{align}

for each $s^{th}$ equation in (\ref{system2}). Equation (\ref{generalmatrix}) is solved sequentially. It is solved for $\textbf{X}_1$ first, using the initial conditions given. Then, the obtained result is used to solve  (\ref{generalmatrix}) for $\textbf{X}_2$. Similarly, for $\textbf{X}_3$, the values of $\textbf{X}_1$ and $\textbf{X}_2$ obtained are used. The same procedure is used for $\textbf{X}_s$ for $s =  4,5, \ldots, m.$
%Notice that no Lemmas are required in the proof of Theorem \ref{thm:jwt}.

%Use \textbackslash ref for tables, figures, theorems, etc. and \textbackslash eqref for equations.

%Use \textbackslash ldots for continuation of commas $,\ldots,$ and \textbackslash cdots for continuation of operators $\times\cdots\times$.

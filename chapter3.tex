\chapter{Numerical Experiments}\label{Chapter3}

To demonstrate the  accuracy of the proposed method in solving non-linear Initial Value Problems (IVPs), we consider several chaotic systems, namely; Lorenz attractor, Genesio-Tesi system, R\"ossler attractor, and Chua's circuit system. 


\section{Lorenz attractor}
According to \citet{ghys2013lorenz}, ``the Lorenz dynamics feature an ensemble of qualitative phenomena which are thought, today, to be presenting {\em generic} dynamics". Modelling thermal variation in a fluid cell between two parallel plates, \citet{lorenz1963deterministic} derived the non-linear differential equations describing the Lorenz system as

\begin{align}
\frac{dx}{dt} &= a(y-x), \qquad x(0) = x_0,\label{lorenzeq1}\\
\frac{dy}{dt} &= -xz + bx -y, \qquad y(0) = y_0,\label{lorenzeq2}\\
\frac{dz}{dt} &= xy - cz, \qquad z(0) = z_0,\label{lorenzeq3}
\end{align}  

where $a, b, c$ are non-negative parameters. In this project, we used $x_0 = 1, y_0 = 5, z_0 = 10, a=10, b=28,$ and $c= \frac{8}{3}$. Applying the relaxation technique to the Lorenz system in each subinterval $\Omega_k$, we obtain

\begin{align}
\frac{dx_{r+1}^k}{dt} + ax_{r+1}^k &= ay_r^k,\\
\frac{dy_{r+1}^k}{dt} + y_{r+1}^k  &= -x_{r+1}^k z_r^k + bx_{r+1}^k,\\
\frac{dz_{r+1}^k}{dt} + cz_{r+1}^k &= x_{r+1}^k y_{r+1}^k.
\end{align}

Evaluating the system at the Chebyshev-Gauss-Lobatto collocation points, $\tau_j$, and using the Chebyshev differentiation matrix to approximate the derivatives, we have

\begin{align}
\frac{dx_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~x_{i, r+1}^k, \\ \frac{dy_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~y_{i, r+1}^k, \\ \text{and} \quad \frac{dz_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~z_{i, r+1}^k. 
\end{align}

Hence we obtain the system 

\begin{align}
&\sum_{i=0}^N D_{ji}~x_{i, r+1}^k + ax_{j, r+1}^k = ay_{j,r}^k, \label{lorenzsyst1}\\
&\sum_{i=0}^N D_{ji}~y_{i, r+1}^k + y_{j, r+1}^k  = -x_{j,r+1}^k z_{j,r}^k + bx_{j,r+1},\label{lorenzsyst2}\\
&\sum_{i=0}^N D_{ji}~z_{i, r+1}^k + cz_{j,r+1}^k = x_{j,r+1}^k y_{j,r+1}^k,\label{lorenzsyst3}
\end{align}

for $j = 0,1,\ldots,N$. Since 

\begin{align}
x(0) = x(\tau_N) &= x_N = 1,\\
y(0) = y(\tau_N) &= y_N = 5,\\
z(0) = z(\tau_N) &= z_N = 10,
\end{align}

then Equations (\ref{lorenzsyst1}) - (\ref{lorenzsyst3}) can be expressed as

\begin{align}
&\sum_{i=0}^{N-1} D_{ji}~x_{i, r+1}^k + ax_{j, r+1}^k = ay_{j,r}^k - D_{jN}, \label{lorenzsyst11}\\
&\sum_{i=0}^{N-1} D_{ji}~y_{i, r+1}^k + y_{j, r+1}^k  = -x_{j,r+1}^k z_{j,r}^k + bx_{j,r+1}^k - 5D_{jN} ,\label{lorenzsyst22}\\
&\sum_{i=0}^{N-1} D_{ji}~z_{i, r+1}^k + cz_{j,r+1}^k = x_{j,r+1}^k y_{j,r+1}^k - 10D_{jN} ,\label{lorenzsyst33}
\end{align}

for $j=0,1,\ldots,N-1$. Let  $ ay_{j,r}^k - D_{jN}$, $ -x_{j,r+1}^k z_{j,r}^k + bx_{j,r+1}^k - 5D_{jN}$ and $x_{j,r+1}^k y_{j,r+1}^k - 10D_{jN}$, be denoted by $Rx_{j,y}$, $Ry_{j,y}$ and $Rz_{j,y}$, respectively. Then each equation in the system (\ref{lorenzsyst11}) - (\ref{lorenzsyst33}) can be represented in a matrix equation form; for Equation (\ref{lorenzsyst11}) we have

\begin{align}\label{lorenzmatrix1}
\begin{bmatrix}
D_{00} + a & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + a & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + a & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + a
\end{bmatrix} \begin{bmatrix}
x_{0,r+1}^k\\
x_{1,r+1}^k\\
x_{2,r+1}^k\\
\vdots\\
x_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rx_{0,r}^k\\
Rx_{1,r}^k\\
Rx_{2,r}^k\\
\vdots\\
Rx_{N-1,r}^k\\
\end{bmatrix},
\end{align}

which is solved for $\textbf{X}_{r+1}^k$ in each interval. For Equation (\ref{lorenzsyst22}) we have

\begin{align}\label{lorenzmatrix2}
\begin{bmatrix}
D_{00} + 1 & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + 1 & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + 1 & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + 1
\end{bmatrix} \begin{bmatrix}
y_{0,r+1}^k\\
y_{1,r+1}^k\\
y_{2,r+1}^k\\
\vdots\\
y_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Ry_{0,r}^k\\
Ry_{1,r}^k\\
Ry_{2,r}^k\\
\vdots\\
Ry_{N-1,r}^k\\
\end{bmatrix},
\end{align}

solved for $\textbf{Y}_{r+1}^k$ using the values of $\textbf{X}_{r+1}^k$ obtained from (\ref{lorenzmatrix1}), in each interval. Lastly, for Equation (\ref{lorenzsyst33}) we have

\begin{align}\label{lorenzmatrix3}
\begin{bmatrix}
D_{00} + c & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + c & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + c & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + c
\end{bmatrix} \begin{bmatrix}
z_{0,r+1}^k\\
z_{1,r+1}^k\\
z_{2,r+1}^k\\
\vdots\\
z_{N-1,r+1}
\end{bmatrix} ~ = \begin{bmatrix}
Rz_{0,r}^k\\
Rz_{1,r}^k\\
Rz_{2,r}^k\\
\vdots\\
Rz_{N-1,r}^k\\
\end{bmatrix},
\end{align}

solved using the values obtained from Equation (\ref{lorenzmatrix1}) and (\ref{lorenzmatrix2}). Hence we say the method employs a Gauss-Seidel relaxation technique. The matrix systems are solved in $\bigtriangleup t = 0.01$ intervals discretized with $10$ collocation points.  
 
\section{Genesio system}
The Genisio system is given by the non-linear differential equations

\begin{align}
\frac{dx}{dt} &= y, \qquad x(0) = x_0,\\
\frac{dy}{dt} &= z, \qquad y(0) = y_0,\\
\frac{dz}{dt} &= -cx -by - az + x^2, \qquad z(0) = z_0.
\end{align}

A chaotic behaviour is observed in the system when $a = 1.2,~ b = 2.92,~ c = 6$ ~\citep{genesio1992harmonic}. In this study we used the same values and subjected the system to the initial conditions; $x_0 = 0.2, y_0 = -0.3, z_0 = 0.1$. Applying the relaxation technique to the Genesio system in each subinterval, $\Omega_k$, we obtain

\begin{align}
&\frac{dx_{r+1}^k}{dt} = y_r^k,\\
&\frac{dy_{r+1}^k}{dt} = z_r^k,\\
&\frac{dz_{r+1}^k}{dt} + az_{r+1}^k = -cx_{r+1}^k -by_{r+1}^k + x^{2^{(k)}}_{r+1}.
\end{align}

Evaluating the system at the Chebyshev-Gauss-Lobatto collocation points, $\tau_j$, using the Chebyshev differentiation matrix, we have the derivatives approximated as

\begin{align}
\frac{dx_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~x_{i,r+1}^k, \\ \frac{dy_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~y_{i,r+1}^k, \\ \text{and} \quad \frac{dz_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~z_{i, r+1}^k. 
\end{align}

Hence we obtain the system 

\begin{align}
&\sum_{i=0}^N D_{ji}~x_{i, r+1}^k = y_{j,r}^k, \label{genesiosyst1}\\
&\sum_{i=0}^N D_{ji}~y_{i, r+1}^k  = z_{j,r}^k,\label{genesiosyst2}\\
&\sum_{i=0}^N D_{ji}~z_{i, r+1}^k + az_{j,r+1}^k = -cx_{j,r+1}^k -by_{j,r+1}^k + x^{2^{(k)}}_{j,r+1},\label{genesiosyst3}
\end{align}

for $j = 0,1,\ldots,N$. From the initial conditions

\begin{align}
x(0) = x(\tau_N) &= x_N = 0.2,\\
y(0) = y(\tau_N) &= y_N = -0.3,\\
z(0) = z(\tau_N) &= z_N = 0.1,
\end{align}

we can then write the system (\ref{genesiosyst1}) - (\ref{genesiosyst3}) as follows

\begin{align}
&\sum_{i=0}^{N-1} D_{ji}~x_{i, r+1}^k = y_{j,r}^k - 0.2D_{jN}, \label{genesiosyst11}\\
&\sum_{i=0}^{N-1} D_{ji}~y_{i, r+1}^k = z_{j,r}^k + 0.3D_{jN},\label{genesiosyst22}\\
&\sum_{i=0}^{N-1} D_{ji}~z_{i, r+1}^k + az_{j,r+1}^k = -cx_{j,r+1}^k -by_{j,r+1}^k + x^{2^{(k)}}_{j,r+1}- 0.1D_{jN} ,\label{genesiosyst33}
\end{align}

for $j=0,1,\ldots,N-1$. Denoting $ y_{j,r}^k - 0.2D_{jN}~$, $ z_{j,r}^k + 0.3D_{jN}$ and $-cx_{j,r+1}^k -by_{j,r+1}^k + x^{2^{(k)}}_{j,r+1} - 0.1D_{jN}$, by $Rx_{j,y}$, $Ry_{j,y}$ and $Rz_{j,y}$, respectively, then each equation in the system (\ref{genesiosyst11}) - (\ref{genesiosyst33}) can be represented in a matrix equation form. For Equation (\ref{genesiosyst11}) we have

\begin{align}\label{genesiomatrix1}
\begin{bmatrix}
D_{00}  & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11}  & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22}  & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} 
\end{bmatrix} \begin{bmatrix}
x_{0,r+1}^k\\
x_{1,r+1}^k\\
x_{2,r+1}^k\\
\vdots\\
x_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rx_{0,r}^k\\
Rx_{1,r}^k\\
Rx_{2,r}^k\\
\vdots\\
Rx_{N-1,r}^k\\
\end{bmatrix}.
\end{align}

Equation (\ref{genesiomatrix1}) is solved for $\textbf{X}_{r+1}^k$. For Equation (\ref{genesiosyst22}) we have

\begin{align}\label{genesiomatrix2}
\begin{bmatrix}
D_{00}  & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11}  & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22}  & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} 
\end{bmatrix} \begin{bmatrix}
y_{0,r+1}^k\\
y_{1,r+1}^k\\
y_{2,r+1}^k\\
\vdots\\
y_{N-1,r+1}
\end{bmatrix} ~ = \begin{bmatrix}
Ry_{0,r}^k\\
Ry_{1,r}^k\\
Ry_{2,r}^k\\
\vdots\\
Ry_{N-1,r}^k\\
\end{bmatrix},
\end{align}

which is solved for $\textbf{Y}_{r+1}^k$ using the values of $\textbf{X}_{r+1}^k$ obtained from (\ref{genesiomatrix2}). Lastly, for Equation (\ref{genesiosyst33}) we have

\begin{align}\label{genesiomatrix3}
\begin{bmatrix}
D_{00} + a & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + a & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + a & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + a
\end{bmatrix} \begin{bmatrix}
z_{0,r+1}^k\\
z_{1,r+1}^k\\
z_{2,r+1}^k\\
\vdots\\
z_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rz_{0,r}^k\\
Rz_{1,r}^k\\
Rz_{2,r}^k\\
\vdots\\
Rz_{N-1,r}^k\\
\end{bmatrix},
\end{align}

solved using the values from (\ref{genesiomatrix1}) and (\ref{genesiomatrix2}). The equations are solved in $\bigtriangleup t = 0.01$ intervals discretized with $10$ collocation points. 

\section{R\"{o}ssler system}
Inspired by the geometry of flows in three dimension and, in particular, the re-injection principle, \citep{gaspard2005rossler}, R\"ossler invented a series of systems ~\citep{rossler1979continuous}, the most prominent one being 

\begin{align}\label{Rossler syst}
\frac{dx}{dt} &= -(y+z), \qquad x(0) = x_0,\nonumber\\
\frac{dy}{dt} &= x + ay, \qquad y(0) = y_0,\nonumber\\
\frac{dz}{dt} &= b + xz - cz, \qquad z(0) = z_0.\nonumber\\
\end{align}  

A continuous-time dynamical system  exhibiting chaotic behaviour associated with fractal properties of the attractor for selected values of $a, b, c$, can be defined by the R{\"o}ssler system ~\citep{gaspard2005rossler}. In this study, we solved (\ref{Rossler syst}) subject to $x_0 = 0, ~y_0 =0,~ z_0 = 0$ with the parameters $a = 0.2, ~b= 0.2$ and $c = 5.7$.

Applying the MD-SRM to the R{\"o}ssler system (\ref{Rossler syst}), we obtain

\begin{align}\label{Rossler syst2}
&\frac{dx_{r+1}}{dt} = -(y_r + z_r),\nonumber\\
&\frac{dy_{r+1}}{dt} - ay_{r+1} = x_{r+1},\nonumber\\
&\frac{dz_{r+1}}{dt} + cz_{r+1} = b + x_{r+1}z_r.\nonumber\\
\end{align}

Evaluating System (\ref{Rossler syst2}) at the Chebyshev-Gauss-Lobatto collocation points $\tau_j$, in terms of the Chebyshev differentiation matrix, we have the derivatives approximated as

\begin{align}
\frac{dx_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~x_{i}^k, \\ \frac{dy_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~y_{i}^k, \\ \text{and} \quad \frac{dz_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~z_{i}^k. 
\end{align}

Hence we obtain the system 

\begin{align}
&\sum_{i=0}^N D_{ji}~x_{i, r+1}^k = -(y_{j,r}^k + z_{j,r}^k), \label{rosslersyst1}\\
&\sum_{i=0}^N D_{ji}~y_{i, r+1}^k - ay_{j, r+1}^k = x_{j,r+1}^k,\label{rosslersyst2}\\
&\sum_{i=0}^N D_{ji}~z_{i, r+1}^k + cz_{j,r+1}^k = b + x_{j,r+1}^k z_{j,r}^k,\label{rosslersyst3}
\end{align}

for $j=0,1,\ldots,N$. From the initial conditions

\begin{align}
x(0) = x(\tau_N) &= x_N = 0,\\
y(0) = y(\tau_N) &= y_N = 0,\\
z(0) = z(\tau_N) &= z_N = 0,
\end{align}

we can then write the system (\ref{rosslersyst1}) - (\ref{rosslersyst3}) as follows

\begin{align}
&\sum_{i=0}^{N-1} D_{ji}~x_{i, r+1}^k = -(y_{j,r}^k + z_{j,r}^k), \label{rosslersyst11}\\
&\sum_{i=0}^{N-1} D_{ji}~y_{i, r+1}^k - ay_{r+1}^k = x_{j,r+1}^k,\label{rosslersyst22}\\
&\sum_{i=0}^{N-1} D_{ji}~z_{i, r+1}^k + cz_{j,r+1}^k = b + x_{j,r+1}^k z_{j,r}^k ,\label{rosslersyst33}
\end{align}

for $j=0,1,\ldots,N-1$. Let the right hand side of each equation above; $ -(y_{j,r}^k + z_{j,r}^k)$, $ x_{j,r+1}^k$ and $b + x_{j,r+1}^k z_{j,r}^k$, be denoted by $Rx_{j,y}$, $Ry_{j,y}$ and $Rz_{j,y}$, respectively. Then each equation in the system (\ref{rosslersyst11}) - (\ref{rosslersyst33}) can be represented in a matrix equation form. For Equation (\ref{rosslersyst11}) we have

\begin{align}\label{rosslermatrix1}
\begin{bmatrix}
D_{00}  & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11}  & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22}  & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} 
\end{bmatrix} \begin{bmatrix}
x_{0,r+1}^k\\
x_{1,r+1}^k\\
x_{2,r+1}^k\\
\vdots\\
x_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rx_{0,r}^k\\
Rx_{1,r}^k\\
Rx_{2,r}^k\\
\vdots\\
Rx_{N-1,r}^k\\
\end{bmatrix},
\end{align}

which is solved for $\textbf{X}_{r+1}^k$. Then, for Equation (\ref{rosslersyst22}) we have

\begin{align}\label{rosslermatrix2}
\begin{bmatrix}
D_{00} - a  & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} - a & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} - a  & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} - a 
\end{bmatrix} \begin{bmatrix}
y_{0,r+1}^k\\
y_{1,r+1}^k\\
y_{2,r+1}^k\\
\vdots\\
y_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Ry_{0,r}^k\\
Ry_{1,r}^k\\
Ry_{2,r}^k\\
\vdots\\
Ry_{N-1,r}^k\\
\end{bmatrix},
\end{align}

solved for $\textbf{Y}_{r+1}^k$ using $\textbf{X}_{r+1}^k$ obtained from (\ref{rosslermatrix1}), and lastly, for Equation (\ref{rosslersyst33}) we have

\begin{align}\label{rosslermatrix3}
\begin{bmatrix}
D_{00} + c & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + c & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + c & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + c
\end{bmatrix} \begin{bmatrix}
z_{0,r+1}^k\\
z_{1,r+1}^k\\
z_{2,r+1}^k\\
\vdots\\
z_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rz_{0,r}^k\\
Rz_{1,r}^k\\
Rz_{2,r}^k\\
\vdots\\
Rz_{N-1,r}^k\\
\end{bmatrix}.
\end{align}

solved using solutions from (\ref{rosslermatrix1}) and (\ref{rosslermatrix2}). The equations are then solved in $\bigtriangleup t = 0.01$ intervals discretized with $10$ collocation points. 

\section{Chua's circuit system}
Invented in the fall of $1983$ ~\citep{changchaos}, chaotic behaviour in Chua's circuit was confirmed in $1984$ ~\citep{Chua:2007}. These simple electronic circuit, shown in Figure \ref{chuas}, was developed by Leon Chua.

\begin{figure}[!h]\centering 
\includegraphics[width=0.5\textwidth]{images/chuas_circuit_diagram.png}
\caption{Basic chua's circuit schematic ~\citep{chua:com}}
\label{chuas} 
\end{figure}

More designs have been developed, for instance, in secure communication systems and simulations of brain dynamics experiments, by modifying the basic circuit.  In this study, we consider the dimensionless Chua's circuit, ~\citep{chua:com}, described by the following system of non-linear differential equations

\begin{align}\label{chuas syst}
\frac{dx}{dt} &= \alpha(y - x - g(x)), \qquad x(0) = x_0,\\
\frac{dy}{dt} &= x - y + z, \qquad y(0) = y_0,\\
\frac{dz}{dt} &= -\beta y, \qquad z(0) = z_0,
\end{align}

where $g(x)$ is the piecewise-linear function, shown in Figure \ref{chuasfunction} depicting the change in resistance vs current across the Chua Diode.

\begin{figure}[H]\centering 
\includegraphics[width=0.4\textwidth]{images/chuafigures/chuas_diode_nonlinear_resistor_graph_nondimensional.jpg}
\caption{Chua's diode resistance graph ~\citep{chua:com}}
\label{chuasfunction} 
\end{figure}

Numerically, the function is 
\begin{align}
g(x) = mlx + 0.5(m0 - ml)(|x+1| - |x-1|). 
\end{align}

where $m0$ denotes the slope of the two outer segments and $ml$ is the slope of the middle segment in Figure \ref{chuasfunction}. In this study, we used the following values for the parameters: $\alpha = 15.6,~ \beta = 28,~ m0 = -1.143~, ml = -0.715$. The system was solved subject to the initial conditions $x_0 = 0.7,~ y_0 = 0, z_0 = 0$. Let $h' = 0.5(m0 - ml)(|x+1| - |x-1|)$, applying the MD-SRM to Chua's circuit system we obtain

\begin{align}
&\frac{dx_{r+1}}{dt} + (\alpha + \alpha ml) x_{r+1}  = \alpha y_r - \alpha h',\\
&\frac{dy_{r+1}}{dt} + y_{r+1}  = x_{r+1} + z_r,\\
&\frac{dz_{r+1}}{dt} = -\beta y_{r+1}.
\end{align}

Evaluating the system at the Chebyshev-Gauss-Lobatto collocation points,  $\tau_j$, using the Chebyshev differentiation matrix, we  have the derivatives approximated as follows

\begin{align}
\frac{dx_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~x_{i}^k, \\ \frac{dy_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~y_{i}^k, \\ \text{and} \quad \frac{dz_{r+1}^k}{dt} \bigg|_{t=\tau_j} &= \sum_{i=0}^N D_{ji}~z_{i}^k. 
\end{align}

Hence we obtain the system 

\begin{align}
&\sum_{i=0}^N D_{ji}~x_{i, r+1}^k + (\alpha + \alpha ml)x_{j, r+1}^k = \alpha y_{j,r}^k - \alpha h^{'^{(k)}}_{j,r}, \label{chuasyst1}\\
&\sum_{i=0}^N D_{ji}~y_{i, r+1}^k + y_{j, r+1}^k  = x_{j,r+1}^k + z_{j,r}^k,\label{chuasyst2}\\
&\sum_{i=0}^N D_{ji}~z_{i, r+1}^k = -\beta y_{j,r+1}^k,\label{chuasyst3}
\end{align}

for $j = 0,1,\ldots,N$. From the initial conditions 

\begin{align}
x(0) = x(\tau_N) &= x_N = 0.7,\\
y(0) = y(\tau_N) &= y_N = 0,\\
z(0) = z(\tau_N) &= z_N = 0,
\end{align}

the system (\ref{chuasyst1}) - (\ref{chuasyst3}) can be expressed as follows

\begin{align}
&\sum_{i=0}^{N-1} D_{ji}~x_{i, r+1}^k + (\alpha + \alpha ml) x_{j, r+1}^k = \alpha y_{j,r}^k -\alpha h^{'^{(k)}}_{j,r} - 0.7D_{jN}, \label{chuasyst11}\\
&\sum_{i=0}^{N-1} D_{ji}~y_{i, r+1}^k + y_{j, r+1}^k  = x_{j,r+1}^k + z_{j,r}^k ,\label{chuasyst22}\\
&\sum_{i=0}^{N-1} D_{ji}~z_{i, r+1}^k = -\beta y_{j,r+1}^k,\label{chuasyst33}
\end{align}

for $j=0,1,\ldots,N-1$. Let  $ \alpha y_{j,r}^k -\alpha h^{'^{(k)}}_{j,r} - 0.7D_{jN}$, $ x_{j,r+1}^k + z_{j,r}^k$ and $-\beta y_{j,r+1}^k$, be denoted by $Rx_{j,y}$, $Ry_{j,y}$ and $Rz_{j,y}$, respectively. Then each equation in the system (\ref{chuasyst11}) - (\ref{chuasyst33}) can be represented in a matrix equation form; for Equation (\ref{chuasyst11}) we have

\begin{align}\label{chuamatrix1}
\begin{bmatrix}
D_{00} + \alpha(1 + ml) & D_{01}  & \cdots & D_{0~N-1}\\
D_{10} & D_{11} +  \alpha(1 + ml) & \cdots & D_{1~N-1}\\
D_{20} & D_{21}  & \cdots & D_{2~N-1}\\
\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & \cdots & D_{N-1~N-1} +  \alpha(1 + ml)
\end{bmatrix} \begin{bmatrix}
x_{0,r+1}^k\\
x_{1,r+1}^k\\
x_{2,r+1}^k\\
\vdots\\
x_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rx_{0,r}^k\\
Rx_{1,r}^k\\
Rx_{2,r}^k\\
\vdots\\
Rx_{N-1,r}^k\\
\end{bmatrix},
\end{align}

which is solved for $\textbf{X}_{r+1}^k$. For Equation (\ref{chuasyst22}) we have

\begin{align}\label{chuamatrix2}
\begin{bmatrix}
D_{00} + 1 & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + 1 & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + 1 & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + 1
\end{bmatrix} \begin{bmatrix}
y_{0,r+1}^k\\
y_{1,r+1}^k\\
y_{2,r+1}^k\\
\vdots\\
y_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Ry_{0,r}^k\\
Ry_{1,r}^k\\
Ry_{2,r}^k\\
\vdots\\
Ry_{N-1,r}^k\\
\end{bmatrix},
\end{align}

solved for $\textbf{Y}_{r+1}^k$ using values from (\ref{chuamatrix1}) and lastly, for Equation (\ref{chuasyst33}) we have

\begin{align}\label{chuamatrix3}
\begin{bmatrix}
D_{00} + 1 & D_{01} & D_{02} & \cdots & D_{0~N-1}\\
D_{10} & D_{11} + 1 & D_{12} & \cdots & D_{1~N-1}\\
D_{20} & D_{21} & D_{22} + 1 & \cdots & D_{2~N-1}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
D_{N-1~0} & D_{N-1~1} & D_{N-1~2} & \cdots & D_{N-1~N-1} + 1
\end{bmatrix} \begin{bmatrix}
z_{0,r+1}^k\\
z_{1,r+1}^k\\
z_{2,r+1}^k\\
\vdots\\
z_{N-1,r+1}^k
\end{bmatrix} ~ = \begin{bmatrix}
Rz_{0,r}^k\\
Rz_{1,r}^k\\
Rz_{2,r}^k\\
\vdots\\
Rz_{N-1,r}^k\\
\end{bmatrix}.
\end{align}

solved using the values obtained in (\ref{chuamatrix1}) and (\ref{chuamatrix2}). The matrix systems are solved in $\bigtriangleup t = 0.001$ interval widths discretized with $10$ collocation points. 
